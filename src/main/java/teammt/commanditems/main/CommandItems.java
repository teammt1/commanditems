package teammt.commanditems.main;

import org.bukkit.plugin.java.JavaPlugin;

import masecla.mlib.main.MLib;
import teammt.commanditems.commands.CommandItemsCommand;
import teammt.commanditems.listeners.InteractionChecker;
import teammt.commanditems.managers.CommandItemManager;

public class CommandItems extends JavaPlugin {

	private MLib lib;

	private CommandItemManager itemManager;

	@Override
	public void onEnable() {
		this.lib = new MLib(this);
		lib.config().requireAll();


		this.itemManager = new CommandItemManager(lib);
		this.itemManager.register();

		new CommandItemsCommand(lib, itemManager).register();
		new InteractionChecker(lib, itemManager).register();
	}
}
