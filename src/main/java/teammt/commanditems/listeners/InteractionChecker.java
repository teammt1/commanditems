package teammt.commanditems.listeners;

import java.util.Set;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import masecla.mlib.classes.Registerable;
import masecla.mlib.main.MLib;
import teammt.commanditems.managers.CommandItemManager;
import teammt.commanditems.managers.model.CommandItem;

public class InteractionChecker extends Registerable {

	private CommandItemManager itemManager;

	public InteractionChecker(MLib lib, CommandItemManager itemManager) {
		super(lib);
		this.itemManager = itemManager;
	}

	@EventHandler
	public void onClick(PlayerInteractEvent event) {
		if (!event.getAction().toString().contains("CLICK"))
			return;

		Player player = (Player) event.getPlayer();

		ItemStack is = event.getItem();
		Set<String> itemNames = this.itemManager.getItems();
		for (String item : itemNames) {
			CommandItem ci = this.itemManager.getItem(item);
			if (ci.matches(is) && ci.matchesEvent(event)) {
				if (ci.getRequiredPermission() != null) {
					if (!player.hasPermission(ci.getRequiredPermission())) {
						event.setCancelled(true);
						lib.message().sendMessage("no-permission-item", player);
						return;
					}
				}

				if (ci.isEventCancel())
					event.setCancelled(true);

				if (ci.getCooldown() != 0) {
					if (this.itemManager.isOnCooldown(event.getPlayer(), item)) {
						lib.message().sendMessage("item-on-cooldown", event.getPlayer());
						return;
					}

					this.itemManager.setOnCooldown(event.getPlayer(), item, ci.getCooldown());
				}

				if (ci.isConsume())
					consumeHandItem(event, is);
				ci.executeCommands(lib, event.getPlayer());
			}
		}
	}

	@SuppressWarnings("deprecation")
	private void consumeHandItem(PlayerInteractEvent event, ItemStack is) {
		if (is.getAmount() == 1) {
			if (lib.compatibility().getServerVersion().getMajor() < 9)
				event.getPlayer().setItemInHand(new ItemStack(Material.AIR));
			else {
				if (event.getHand().equals(EquipmentSlot.HAND))
					event.getPlayer().getInventory().setItemInMainHand(new ItemStack(Material.AIR));
				else
					event.getPlayer().getInventory().setItemInOffHand(new ItemStack(Material.AIR));
			}
		} else
			is.setAmount(is.getAmount() - 1);
	}

}
