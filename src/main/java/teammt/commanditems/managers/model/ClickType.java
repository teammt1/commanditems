package teammt.commanditems.managers.model;

import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

// private boolean left;
// private boolean right;
// private boolean middle;

// private boolean shift;
// private boolean air;

@NoArgsConstructor
@AllArgsConstructor
public enum ClickType {
    RIGHT_CLICK_AIR(false, true, false, true, false),
    RIGHT_CLICK_BLOCK(false, true, false, false, true),
    LEFT_CLICK_AIR(true, false, false, true, false),
    LEFT_CLICK_BLOCK(true, false, false, false, true),
    SHIFT_RIGHT_CLICK_AIR(false, true, true, true, false),
    SHIFT_RIGHT_CLICK_BLOCK(false, true, true, false, true),
    SHIFT_LEFT_CLICK_AIR(true, false, true, true, false),
    SHIFT_LEFT_CLICK_BLOCK(true, false, true, false, true),
    ALL {
        @Override
        public boolean shouldAllow(Player player, PlayerInteractEvent event) {
            return true;
        }
    };

    private boolean left;
    private boolean right;

    private boolean shift;
    private boolean air;
    private boolean block;

    public boolean shouldAllow(Player player, PlayerInteractEvent event) {
        boolean left = event.getAction().equals(Action.LEFT_CLICK_AIR)
                || event.getAction().equals(Action.LEFT_CLICK_BLOCK);
        boolean right = event.getAction().equals(Action.RIGHT_CLICK_AIR)
                || event.getAction().equals(Action.RIGHT_CLICK_BLOCK);
        boolean shift = player.isSneaking();

        boolean air = event.getAction().equals(Action.LEFT_CLICK_AIR)
                || event.getAction().equals(Action.RIGHT_CLICK_AIR);
        boolean block = event.getAction().equals(Action.LEFT_CLICK_BLOCK)
                || event.getAction().equals(Action.RIGHT_CLICK_BLOCK);

        return (this.left == left) && (this.right == right) && (this.shift == shift) && (this.air == air)
                && (this.block == block);
    }
}
