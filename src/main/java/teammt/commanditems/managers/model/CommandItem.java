package teammt.commanditems.managers.model;

import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;
import org.bukkit.profile.PlayerProfile;
import org.bukkit.profile.PlayerTextures;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import lombok.Data;
import masecla.mlib.apis.CompatibilityAPI.Versions;
import masecla.mlib.main.MLib;
import net.md_5.bungee.api.ChatColor;

@Data
public class CommandItem {
    private boolean consume;
    private boolean eventCancel;
    private String requiredPermission;

    private List<ClickType> allowedClicks;

    private Object type;
    private int amount = 1;
    private String name;

    private boolean enchanted = false;
    private List<String> lore;

    private int customModelData;

    private boolean forceOp;
    private List<String> byPlayer;
    private List<String> byConsole;

    private int cooldown;

    private String skullData;

    public CommandItem(MLib lib, ConfigurationSection section) {
        this.consume = section.getBoolean("consume", true);
        this.eventCancel = section.getBoolean("event-cancel", true);
        this.requiredPermission = section.getString("permission", null);

        ConfigurationSection itemData = section.getConfigurationSection("item");
        String type = itemData.getString("type");
        this.type = XMaterial.matchXMaterial(type).orElse(null);
        if (this.type == null)
            this.type = Material.matchMaterial(type);

        List<String> clicks = section.getStringList("allowed-clicks");
        this.allowedClicks = new ArrayList<>();
        for (String click : clicks) {
            try {
                this.allowedClicks.add(ClickType.valueOf(click));
            } catch (IllegalArgumentException e) {
                lib.logger().warn("Click type: " + click + " is not a valid click type!");
            }
        }

        if (type == null) {
            lib.logger().warn("Material type: " + itemData.getString("type") + " is not a valid material!");
        }

        this.amount = itemData.getInt("amount", 1);
        this.name = itemData.getString("name");
        this.lore = itemData.getStringList("lore");
        this.customModelData = itemData.getInt("custom-model-data", 0);
        this.enchanted = itemData.getBoolean("enchanted", false);

        ConfigurationSection commands = section.getConfigurationSection("commands");

        this.forceOp = commands.getBoolean("forceOp", false);

        this.byPlayer = commands.getStringList("byplayer");
        this.byConsole = commands.getStringList("byconsole");

        this.cooldown = section.getInt("cooldown", 0);

        this.skullData = section.getString("skull-data", null);

        if (skullData != null && !this.type.equals(XMaterial.PLAYER_HEAD)) {
            lib.logger().warn("Skull data is only applicable to PLAYER_HEAD items!");
        }
    }

    public void saveTo(ConfigurationSection section) {
        section.set("consume", this.consume);
        section.set("event-cancel", this.eventCancel);
        section.set("permission", this.requiredPermission);

        ConfigurationSection itemData = section.createSection("item");
        if (type instanceof Material)
            itemData.set("type", ((Material) this.type).name());
        else if (type instanceof XMaterial)
            itemData.set("type", ((XMaterial) this.type).name());

        section.set("allowed-clicks", this.allowedClicks.stream().map(ClickType::name).collect(Collectors.toList()));

        itemData.set("amount", this.amount);
        itemData.set("name", this.name);
        itemData.set("lore", this.lore);
        itemData.set("custom-model-data", this.customModelData);
        itemData.set("enchanted", this.enchanted);

        ConfigurationSection commands = section.createSection("commands");
        commands.set("byplayer", this.byPlayer);
        commands.set("byconsole", this.byConsole);
        commands.set("forceOp", this.forceOp);

        section.set("cooldown", this.cooldown);

        section.set("skull-data", this.skullData);
    }

    public boolean matches(ItemStack is) {
        if (is == null || is.getType().equals(Material.AIR))
            return false;

        if (this.type == null)
            return false;

        if (!this.type.equals(XMaterial.matchXMaterial(is)))
            return false;

        if (!isConsume()) {
            if (this.amount == 0) {
                if (is.getAmount() != this.amount)
                    return false;
            }
        }

        if (this.name != null) {
            if (!is.getItemMeta().hasDisplayName())
                return false;

            if (!translateColors(this.name).equals(is.getItemMeta().getDisplayName()))
                return false;
        }

        if (this.lore != null && this.lore.size() != 0) {
            if (!is.getItemMeta().hasLore())
                return false;

            List<String> crLore = is.getItemMeta().getLore();
            if (crLore.size() != this.lore.size())
                return false;

            int i = 0;
            for (String cr : crLore) {
                String convertedLore = translateColors(lore.get(i));
                if (!convertedLore.equals(cr))
                    return false;
                i++;
            }
        }

        if (this.enchanted) {
            if (is.getEnchantments().size() == 0)
                return false;
        }

        return true;
    }

    public boolean matchesEvent(PlayerInteractEvent event) {
        if (this.allowedClicks.size() == 0)
            return true;
        return this.allowedClicks.stream().anyMatch(click -> click.shouldAllow(event.getPlayer(), event));
    }

    private String translateColors(String s) {
        if (s == null)
            return null;
        s = ChatColor.translateAlternateColorCodes('&', s);
        Pattern pattern = Pattern.compile("&#([A-Fa-f0-9]{6})");
        Matcher matcher = pattern.matcher(s);
        while (matcher.find()) {
            String color = matcher.group(1);
            s = s.replace("&#" + color, ChatColor.of("#" + color) + "");
        }

        return s;
    }

    public ItemStack toItemStack(MLib lib) {
        ItemStack is = null;
        if (this.type instanceof XMaterial)
            is = ((XMaterial) this.type).parseItem();
        else if (this.type instanceof Material)
            is = new ItemStack((Material) this.type);
        else
            Bukkit.getLogger().warning("Material type: " + this.type + " is not a valid material!");

        is.setAmount(this.amount);

        ItemMeta meta = is.getItemMeta();
        if (this.name != null)
            meta.setDisplayName(translateColors(this.name));

        if (this.lore != null) {
            List<String> crLore = is.getItemMeta().getLore();
            if (crLore == null)
                crLore = new ArrayList<>();

            for (String cr : this.lore)
                crLore.add(translateColors(cr));
            meta.setLore(crLore);
        }

        if (this.enchanted) {
            meta.addEnchant(Enchantment.DURABILITY, 1, true);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        if (this.skullData != null) {
            if (lib.compatibility().getServerVersion().higherThanOr(Versions.v1_20)) {
                try {
                    URL url = new URL("http://textures.minecraft.net/texture/" + this.skullData);

                    PlayerProfile profile = Bukkit.createPlayerProfile(UUID.randomUUID());
                    PlayerTextures textures = profile.getTextures();

                    textures.setSkin(url);
                    profile.setTextures(textures);
                    ((SkullMeta) meta).setOwnerProfile(profile);
                } catch (MalformedURLException e) {
                    lib.logger().warn("Invalid skull data: " + this.skullData);
                }
            } else {
                String json = "{\"textures\":{\"SKIN\":{\"url\":\"http://textures.minecraft.net/texture/"
                        + this.skullData + "\"}}}";
                String encodedBase64 = Base64.getEncoder().encodeToString(json.getBytes());

                GameProfile profile = new GameProfile(UUID.randomUUID(), "");
                profile.getProperties().put("textures", new Property("textures", encodedBase64));
                Field profileField = null;
                try {
                    profileField = meta.getClass().getDeclaredField("profile");
                    profileField.setAccessible(true);
                    profileField.set(meta, profile);
                } catch (ReflectiveOperationException e) {
                    e.printStackTrace();
                    lib.logger().warn("Failed to set skull data for item!");
                }
            }
        }

        is.setItemMeta(meta);

        if (this.customModelData != 0) {
            is = lib.nms().write().customModelData(this.customModelData).applyOn(is);
        }

        return is;
    }

    public CommandItem(ItemStack is) {
        this.consume = false;
        this.eventCancel = true;

        try {
            this.type = XMaterial.matchXMaterial(is.getType());
        } catch (Exception e) {
            this.type = is.getType();
        }

        this.amount = is.getAmount();
        if (is.getItemMeta().hasDisplayName())
            this.name = is.getItemMeta().getDisplayName();
        if (is.getItemMeta().hasLore())
            this.lore = is.getItemMeta().getLore();

        if (is.getEnchantments().size() != 0)
            this.enchanted = true;
        else
            this.enchanted = false;

        this.byPlayer = new ArrayList<>();
        this.byConsole = new ArrayList<>();

        this.byPlayer.add("say Make sure to change this in the config!");
        this.byConsole.add("say Make sure to change this in the config!");

        this.forceOp = false;

        this.cooldown = 0;
    }

    public void executeCommands(MLib lib, Player p) {
        CommandSender player = forceOp ? wrapOp(p) : p;

        byPlayer.stream().map(c -> lib.placeholder().applyOn(c, p))
                .forEach(c -> Bukkit.dispatchCommand(player, c.replace("%player%", p.getName())));
        byConsole.stream().map(c -> lib.placeholder().applyOn(c, p))
                .forEach(c -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), c.replace("%player%", p.getName())));
    }

    private CommandSender wrapOp(CommandSender sender) {
        return new CommandSender() {

            @Override
            public boolean isPermissionSet(String name) {
                return true;
            }

            @Override
            public boolean isPermissionSet(Permission perm) {
                return true;
            }

            @Override
            public boolean hasPermission(String name) {
                return true;
            }

            @Override
            public boolean hasPermission(Permission perm) {
                return true;
            }

            @Override
            public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value) {
                return sender.addAttachment(plugin, name, value);
            }

            @Override
            public PermissionAttachment addAttachment(Plugin plugin) {
                return sender.addAttachment(plugin);
            }

            @Override
            public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value, int ticks) {
                return sender.addAttachment(plugin, name, value, ticks);
            }

            @Override
            public PermissionAttachment addAttachment(Plugin plugin, int ticks) {
                return sender.addAttachment(plugin, ticks);
            }

            @Override
            public void removeAttachment(PermissionAttachment attachment) {
                sender.removeAttachment(attachment);
            }

            @Override
            public void recalculatePermissions() {
                sender.recalculatePermissions();
            }

            @Override
            public Set<PermissionAttachmentInfo> getEffectivePermissions() {
                return sender.getEffectivePermissions();
            }

            @Override
            public boolean isOp() {
                return true;
            }

            @Override
            public void setOp(boolean value) {
                // ignore
            }

            @Override
            public void sendMessage(String message) {
                sender.sendMessage(message);
            }

            @Override
            public void sendMessage(String... messages) {
                sender.sendMessage(messages);
            }

            @Override
            public void sendMessage(UUID send, String message) {
                sender.sendMessage(send, message);
            }

            @Override
            public void sendMessage(UUID send, String... messages) {
                sender.sendMessage(send, messages);
            }

            @Override
            public Server getServer() {
                return sender.getServer();
            }

            @Override
            public String getName() {
                return sender.getName();
            }

            @Override
            public Spigot spigot() {
                return sender.spigot();
            }
        };
    }
}
