package teammt.commanditems.managers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import masecla.mlib.classes.Registerable;
import masecla.mlib.main.MLib;
import teammt.commanditems.managers.model.CommandItem;

public class CommandItemManager extends Registerable {
    private Map<String, Set<UUID>> onCooldown = new HashMap<>();

    public CommandItemManager(MLib lib) {
        super(lib);
    }

    public CommandItem getItem(String itemName) {
        return new CommandItem(lib, lib.config().getConfig().getConfigurationSection("items." + itemName));
    }

    public boolean itemExists(String itemName) {
        return lib.config().getConfig().isSet("items." + itemName);
    }

    public Set<String> getItems() {
        return lib.config().getConfig().getConfigurationSection("items").getKeys(false);
    }

    public void saveItem(CommandItem item, String itemName) {
        item.saveTo(lib.config().getConfig().createSection("items." + itemName));
        lib.config().updateFile("config");
    }

    public boolean isOnCooldown(Player player, String itemName) {
        return onCooldown.containsKey(itemName) && onCooldown.get(itemName).contains(player.getUniqueId());
    }

    public void setOnCooldown(Player player, String itemName, int cooldown) {
        if (!onCooldown.containsKey(itemName))
            onCooldown.put(itemName, new HashSet<>());
        onCooldown.get(itemName).add(player.getUniqueId());
        Bukkit.getScheduler().runTaskLater(lib.getPlugin(), () -> {
            onCooldown.get(itemName).remove(player.getUniqueId());
        }, cooldown * 20);
    }
}
