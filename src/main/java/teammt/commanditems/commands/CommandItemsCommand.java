package teammt.commanditems.commands;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import masecla.mlib.annotations.RegisterableInfo;
import masecla.mlib.annotations.SubcommandInfo;
import masecla.mlib.classes.Registerable;
import masecla.mlib.classes.Replaceable;
import masecla.mlib.main.MLib;
import teammt.commanditems.managers.CommandItemManager;
import teammt.commanditems.managers.model.CommandItem;

@RegisterableInfo(command = "commanditems")
public class CommandItemsCommand extends Registerable {

	private CommandItemManager itemManager;

	public CommandItemsCommand(MLib lib, CommandItemManager itemManager) {
		super(lib);
		this.itemManager = itemManager;
	}

	@SubcommandInfo(subcommand = "help", permission = "teammt.commanditems.help")
	public void onHelp(CommandSender sender) {
		lib.message().sendMessage("help-message", sender);
	}

	@SubcommandInfo(subcommand = "", permission = "teammt.commanditems.help")
	public void onHelpNoArgs(CommandSender sender) {
		onHelp(sender);
	}

	@SubcommandInfo(subcommand = "reload", permission = "teammt.commanditems.reload")
	public void onReload(CommandSender sender) {
		lib.message().reloadSharedConfig();
		lib.config().reloadAll();
		lib.message().sendMessage("plugin-reloaded", sender);
	}

	@SuppressWarnings("deprecation")
	private void handlePlaceConfig(Player player, String itemName, boolean confirm) {
		ItemStack stack = player.getItemInHand();
		if (stack == null || stack.getType().equals(Material.AIR)) {
			lib.message().sendMessage("no-item-in-hand", player);
			return;
		}

		boolean itemExists = this.itemManager.itemExists(itemName);

		if (itemExists && !confirm) {
			lib.message().sendMessage("item-already-exists", player);
			return;
		}

		CommandItem item = new CommandItem(stack);
		this.itemManager.saveItem(item, itemName);

		lib.message().sendMessage("item-configured", player);
		lib.config().updateFile("config");
	}

	@SubcommandInfo(subcommand = "config", permission = "teammt.commanditems.config")
	public void handlePlaceConfig(Player player, String itemName) {
		handlePlaceConfig(player, itemName, false);
	}

	@SubcommandInfo(subcommand = "config", permission = "teammt.commanditems.config")
	public void handlePlaceConfigConfirm(Player player, String itemName, String confirm) {
		handlePlaceConfig(player, itemName, confirm.equalsIgnoreCase("confirm"));
	}

	@SubcommandInfo(subcommand = "give", permission = "teammt.commanditems.give")
	public void handleGiveItem(Player player, String itemName) {
		if (!itemManager.itemExists(itemName)) {
			lib.message().sendMessage("item-does-not-exist", player);
			return;
		}

		CommandItem item = itemManager.getItem(itemName);
		player.getInventory().addItem(item.toItemStack(lib));
		lib.message().sendMessage("item-given", player);
	}

	@SubcommandInfo(subcommand = "give", permission = "teammt.commanditems.give")
	public void handleGiveItem(CommandSender player, String itemName, String targetName) {
		Player target = Bukkit.getPlayer(targetName);
		if (target == null) {
			lib.message().sendMessage("player-not-found", player);
			return;
		}

		if (!itemManager.itemExists(itemName)) {
			lib.message().sendMessage("item-does-not-exist", player);
			return;
		}

		CommandItem item = itemManager.getItem(itemName);

		target.getInventory().addItem(item.toItemStack(lib));
		lib.message().sendMessage("item-given", player);
		lib.message().sendMessage("item-received", target);
	}

	@SubcommandInfo(subcommand = "types", permission = "teammt.commanditems.types")
	public void handleItemTypes(CommandSender sender) {
		String types = String.join(", ", itemManager.getItems());
		lib.message().sendMessage("item-types", sender, new Replaceable("%types%", types));
	}
}
